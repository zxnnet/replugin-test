import os
import json

APKTOOL_PATH = "/Users/viewv/replugin-test/ReVerify/apktool/apktool"


class ReVerify(object):

    def __init__(self, folder):
        self.apklist = list()
        self.folder = folder
        self.result = dict()

    def get_apk_list(self):
        for file in os.listdir(self.folder):
            if os.path.isfile(os.path.join(self.folder, file)):
                filetype = os.path.splitext(file)
                if filetype[1] == ".apk":
                    print(os.path.join(self.folder, file))
                    self.apklist.append(file)
        print("Get apk list done")

    def extract_host(self):
        for apk in self.apklist:
            print(apk)
            filetype = os.path.splitext(apk)
            command = "sh " + APKTOOL_PATH + " d " + "-f " + \
                os.path.join(self.folder, apk) + " -o " + \
                os.path.join(self.folder, filetype[0])
            print(command)
            os.system(command)
            with open(os.path.join(self.folder, filetype[0], "AndroidManifest.xml")) as f:
                if 'replugin' in f.read():
                    with open(os.path.join(
                            self.folder, filetype[0], "assets", "plugins-builtin.json")) as j:
                        plugins_buildin = json.load(j)
                        self.result[apk] = plugins_buildin

    def start(self):
        self.get_apk_list()
        self.extract_host()


if __name__ == "__main__":
    print("ReVerify")
    folder = input("APKs Folder: ")
    verify = ReVerify(folder)
    verify.start()
